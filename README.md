### REST-ful API for scikit-learn models serving 

#### Deploying sklearn estimator
In order to serve our sklearn model we need to follow 3 simple steps:

0. save our sklearn estimator using `joblib.dump(estimator, path)`
1. add path to `.joblib` file in `model.env`* as `MODEL_HOST_PATH`. It will be convenient to run same container with different models.
2. Build our container using command: 
   
   `docker build --file Dockerfile --tag office_predict .`
   
3. Run newly build microservice on `8000` port and pass `model.env`: 
   
   ` docker run -p 8000:8000 -env_file=./model.env office_predict`

*(moreover, `.env` file prevent us from exposing sensitive credentials straight in code base, or in `docker run ...` command among processe on host machine )

### Retrieving predictions

As soon as container up and running you can use following POST request to retrieve predictions. 
You should specify path to `sample.csv` file on your disk after `csv_file=@`

```
curl -X 'POST' \
  'http://127.0.0.1:8000/predict' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'csv_file=@sample_payload.csv;type=text/csv'
 ```

Alternatively, you can utilize SwaggerUI available (due to `FastAPI`) on `http://127.0.0.1:8000/docs` to choose file from disk and download your predictions as file.

### Bonus: [EDA + model train](https://colab.research.google.com/drive/1Fn925Y58BuCSdmxYJ1qu23lIpnipsxJg#scrollTo=ihslfjpbjtbV)


## TODO
* ~~dockerize~~
* ~~extend README~~
* ~provide comments where necessary~
* tests on input and output
* introduce FeatureBuilder
* serve prediction for files from url 
