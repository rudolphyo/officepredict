from typing import List, Union
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline

from src.features.feature import Feature
from src.features.features_builder import FeaturesBuilder
from src.model_utils import BASELINE_FTRS


def get_pipeline(features: List[Feature] = BASELINE_FTRS) -> Pipeline:
    pipeline = Pipeline(steps=[
        ("features_builder", FeaturesBuilder(features_list=features)),
        ("clf", RandomForestClassifier(max_depth=2, random_state=0))
    ])
    return pipeline


def get_experimental_pipeline(
        experimental_features: Union[Feature, List[Feature]]
) -> Pipeline:
    if isinstance(experimental_features, Feature):
        experimental_features = [experimental_features]
    features = BASELINE_FTRS + experimental_features
    pipeline = get_pipeline(features)
    pipeline.named_steps["features_builder"].set_params(
        _features_list=features
    )
    return pipeline


