import abc
import pandas as pd
from typing import Any, List


class Feature(abc.ABC):
    """
    Your data scientist colleague came up with this artifact to store model features information.
    The class tracks the name of the new feature, the name of the input columns and must expose
    a compute method that takes a DataFrame and returns a list of values
    """
    name: str
    input_columns: List[str]

    @abc.abstractmethod
    def compute(self, X: pd.DataFrame) -> List[Any]:
        raise NotImplementedError
