from typing import List
import pandas as pd
from sklearn.base import TransformerMixin
from sklearn.exceptions import NotFittedError

from src.features.feature import Feature


class FeaturesBuilder(TransformerMixin):
    """
    Create new features by combining existing ones or transform them using some funky math operation.
    :param features_list: a list of features.Feature instances.
        If nothing is passed, the transformer will default to those in the features_library
    Usage:
    >> features_builder = FeaturesBuilder(features_list=[MyFeature1, MyFeature2])
    >> features_builder.fit_transform(X)
    """

    def __init__(self, features_list: List[Feature]):
        self._features_list: List[Feature] = features_list
        self._fitted = False

    def get_params(self, deep=True):
        return {"features_dict": self._features_list}

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)

    def fit(self, X: pd.DataFrame = None, y: pd.DataFrame = None) -> TransformerMixin:
        for f in self._features_list:
            for col in f.input_columns:
                assert col in X.columns
        self._fitted = True
        return self

    def transform(self, X: pd.DataFrame) -> pd.DataFrame:
        """
        :param X: DataFrame with raw features
        :return: input DataFrame plus new features
        """
        _X = X.copy(deep=True)
        if not self._fitted:
            raise NotFittedError()

        for f in self._features_list:
            _X[f.name] = f.compute(X)
        return _X[[f.name for f in self._features_list]]
