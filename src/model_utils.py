import os
import pandas as pd
import joblib
from io import BytesIO

N_FULL_FEATURES = 62
BASELINE_FTRS = ['officearea','comarea','yearbuilt']


def load_model(path=os.environ.get("MODEL_PATH")):
    model = joblib.load(path)
    return model


def get_predictions(model, x):
    x_preprocessed = preprocess(x)
    y = model.predict(x_preprocessed).tolist()
    prob = model.predict_proba(x).tolist()
    return {'prediction': y, 'probability': prob}


def open_csv(csv_file):
    df = pd.read_csv(BytesIO(csv_file.file.read()), index_col="index", low_memory=False)
    return df


def process_csv(df):
    df = df[BASELINE_FTRS]
    return df


def preprocess(x):
    return x.to_numpy()