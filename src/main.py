from typing import List, Dict
from fastapi import FastAPI, File, UploadFile, HTTPException
from pydantic import BaseModel

from src.model_utils import N_FULL_FEATURES, load_model, get_predictions, open_csv, process_csv

app = FastAPI()
model = load_model()


class PredictResponse(BaseModel):
    data: Dict[str, List]


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/predict")
def predict(csv_file: UploadFile = File(...)):
    try:
        input_df = open_csv(csv_file)
    except:
        raise HTTPException(
            status_code=422, detail="Unable to process file"
        )

    if input_df.shape[1] != N_FULL_FEATURES:
        raise HTTPException(
            status_code=422,
            detail=f"Each data point must contain {N_FULL_FEATURES} features",
        )

    clean_df = process_csv(input_df)
    preds = get_predictions(model, clean_df)

    return PredictResponse(data=preds)

